import java.util.ArrayList;
import java.util.List;

/*
 * Main-Klasse zum Testen der Klassen Ladung und Raumschiff
 */
public class RaumschiffeMitLadung {

	public static void main(String[] args) {
		Ladung l1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung l2 = new Ladung("Bat'leth Klingonen Schwert", 200);
		Ladung l3 = new Ladung("Borg-Schrott", 5);
		Ladung l4 = new Ladung("Rote Materie", 2);
		Ladung l5 = new Ladung("Plasma-Waffe", 50);
		Ladung l6 = new Ladung("Forschungssonde", 35);
		Ladung l7 = new Ladung("Photonentorpedo", 3);

		List<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
		ladungsverzeichnis.add(l1);

		Raumschiff klingonen = new Raumschiff("IKS Hegh'ta", 100, 100, 100, 100, 1, 2, ladungsverzeichnis, null);
		Raumschiff romulaner = new Raumschiff("IRW Khazara", 100, 100, 100, 100, 2, 2, null, null);
		Raumschiff vulkanier = new Raumschiff("Ni'Var", 80, 80, 100, 50, 0, 5, null, null);

		klingonen.addLadung(l2);

		romulaner.addLadung(l3);
		romulaner.addLadung(l4);
		romulaner.addLadung(l5);

		vulkanier.addLadung(l6);
		vulkanier.addLadung(l7);

		klingonen.photonentorpedosSchiessen(romulaner);
		romulaner.phaserkanoneSchiessen(klingonen);
		vulkanier.broadcast("Gewalt ist nicht logisch");

		klingonen.zustandAnzeigen();
		klingonen.ladungsverzeichnisAusgeben();

		vulkanier.reparaturauftragSenden(true, true, true, 5);
		vulkanier.photonentorpedosLaden(7);
		vulkanier.ladungsverzeichnisAufraeumen();
		
		for (int i = 0; i < 2; i++) {
			klingonen.photonentorpedosSchiessen(vulkanier);
		}

		klingonen.zustandAnzeigen();
		klingonen.ladungsverzeichnisAusgeben();
		romulaner.zustandAnzeigen();
		romulaner.ladungsverzeichnisAusgeben();
		vulkanier.zustandAnzeigen();
		vulkanier.ladungsverzeichnisAusgeben();

		klingonen.logbuchAusgeben();

	}

}

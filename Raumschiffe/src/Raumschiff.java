import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Klasse um Informationen �ber ein Raumschiff zu speichern und zu verwalten
 * 
 * @author Christoph M.
 *
 */
public class Raumschiff {

	private String schiffsname;
	private double energieversorgungInProzent;
	private double schutzschildeInProzent;
	private double lebenserhaltungssystemeInProzent;
	private double huelleInProzent;
	private int photonentorpedos;
	private int reparaturAndroiden;
	private List<Ladung> ladungsverzeichnis;
	private static ArrayList<String> broadcastKommunikator;
	private ArrayList<String> logbuch;

	/**
	 * leerer Standardkonstruktor der Klasse Raumschiff
	 */
	public Raumschiff() {

	}

	/**
	 * vollparametisierter Konstruktor der Klasse Raumschiff
	 * 
	 * @param schiffsname                      Name des Raumschiffes
	 * @param energieversorgungInProzent       Wert der Energieversorgung des
	 *                                         Raumschiffes in Prozent
	 * @param schutzschildeInProzent           Wert der Schutzschilde des
	 *                                         Raumschiffes in Prozent
	 * @param lebenserhaltungssystemeInProzent Wert der Lebenserhaltungssysteme des
	 *                                         Raumschiffes in Prozent
	 * @param huelleInProzent                  Wert der Huelle des Raumschiffes in
	 *                                         Prozent
	 * @param photonentorpedos                 Anzahl der abschussbereiten
	 *                                         Photonentorpedos
	 * @param reparaturAndroiden               Anzahl der Androiden, die f�r die
	 *                                         Reperatur des Schiffes zust�ndig sind
	 * @param ladungsverzeichnis               Gibt Auskunft ueber die Ladung des
	 *                                         Raumschiffes
	 * @param logbuch                          enthaelt alle Logbucheintraege
	 */
	public Raumschiff(String schiffsname, double energieversorgungInProzent, double schutzschildeInProzent,
			double lebenserhaltungssystemeInProzent, double huelleInProzent, int photonentorpedos,
			int reparaturAndroiden, List<Ladung> ladungsverzeichnis, ArrayList<String> logbuch) {
		setSchiffsname(schiffsname);
		setEnergieversorgungInProzent(energieversorgungInProzent);
		setSchutzschildeInProzent(schutzschildeInProzent);
		setLebenserhaltungssystemeInProzent(lebenserhaltungssystemeInProzent);
		setHuelleInProzent(huelleInProzent);
		setPhotonentorpedos(photonentorpedos);
		setReparaturAndroiden(reparaturAndroiden);

		this.ladungsverzeichnis = new ArrayList<Ladung>();
		if (ladungsverzeichnis != null) {
			if (ladungsverzeichnis.size() != 0) {
				for (int i = 0; i < ladungsverzeichnis.size(); i++) {
					this.ladungsverzeichnis.add(ladungsverzeichnis.get(i));
				}
			}

		}
		this.logbuch = new ArrayList<String>();
		if (logbuch != null) {
			for (int i = 0; i < logbuch.size(); i++) {
				this.logbuch.add(logbuch.get(i));
			}
		}

	}

	/**
	 * zum Hinzufuegen von Ladung zum Schiff, neue Ladung wird im Ladungsverzeichnis
	 * gespeichert
	 * 
	 * @param neueLadung Ladung, die dem Schiff aufgeladen wird
	 */
	public void addLadung(Ladung neueLadung) {
		ladungsverzeichnis.add(neueLadung);
	}

	/**
	 * gibt den aktuelle Zustand des Raumschiffes, also die Eigenschaften des
	 * Schiffes, wieder
	 */
	public void zustandAnzeigen() {
		System.out.printf("%s\n", "Schiffsname: " + this.schiffsname);
		System.out.printf("%s\n", "Energieversorgung: " + this.energieversorgungInProzent + " %");
		System.out.printf("%s\n", "Schutzschilde: " + this.schutzschildeInProzent + " %");
		System.out.printf("%s\n", "Lebenserhaltungssysteme: " + this.lebenserhaltungssystemeInProzent + " %");
		System.out.printf("%s\n", "Huelle: " + this.huelleInProzent + " %");
		System.out.printf("%s\n", "Anzahl Photonentorpedos: " + this.photonentorpedos);
		System.out.printf("%s\n\n", "Anzahl Reparatur-Androiden: " + this.reparaturAndroiden);
	}

	/**
	 * gibt f�r jeden Inhalt des Ladungsverzeichnisses den Typ und die Menge in der
	 * Konsole aus
	 */
	public void ladungsverzeichnisAusgeben() {
		for (int i = 0; i < this.ladungsverzeichnis.size(); i++) {
			System.out.println("Ladung " + (i + 1) + ":");
			System.out.printf("%7s%s\n", "Typ: ", ladungsverzeichnis.get(i).getTyp());
			System.out.printf("%9s%d\n", "Menge: ", ladungsverzeichnis.get(i).getMenge());
		}
		System.out.println("");
	}

	/**
	 * zum Schiessen von Photonentorpedos auf ein gegnerisches Raumschiff
	 * 
	 * @param zielraumschiff Raumschiff, auf das geschossen werden soll
	 */
	public void photonentorpedosSchiessen(Raumschiff zielraumschiff) {
		if (this.photonentorpedos == 0) {
			broadcast("-=*Click*=-");
		} else {
			this.photonentorpedos--;
			System.out.println("Photonentorpedo abgeschossen\n");
			raumschiffGetroffen(zielraumschiff);
		}
	}

	/**
	 * zum Schiessen der Phaserkanone auf ein gegnerisches Raumschiff
	 * 
	 * @param zielraumschiff Raumschiff, auf das geschossen werden soll
	 */
	public void phaserkanoneSchiessen(Raumschiff zielraumschiff) {
		if (this.energieversorgungInProzent < 50) {
			broadcast("-=*Click*=-");
		} else {
			this.energieversorgungInProzent -= 50;
			System.out.println("Phaserkanone abgeschossen\n");
			raumschiffGetroffen(zielraumschiff);
		}
	}

	/**
	 * zur Aktualisierung der Eigenschaftswerte des Raumschiffes, welche
	 * abgeschossen wurde
	 * 
	 * @param getroffenesSchiff Raumschiff, welches soeben von Photonentorpedos oder
	 *                          der Phaserkanone getroffen wurde
	 */
	private void raumschiffGetroffen(Raumschiff getroffenesSchiff) {
		System.out.println(getroffenesSchiff.getSchiffsname() + " wurde getroffen!\n");
		double schilde = getroffenesSchiff.getSchutzschildeInProzent() - 50;
		getroffenesSchiff.setSchutzschildeInProzent(schilde);
		if (schilde <= 0) {
			double energieversorgung = getroffenesSchiff.getEnergieversorgungInProzent() - 50;
			double huelle = getroffenesSchiff.getHuelleInProzent() - 50;
			getroffenesSchiff.setEnergieversorgungInProzent(energieversorgung);
			getroffenesSchiff.setHuelleInProzent(huelle);

			if (getroffenesSchiff.getHuelleInProzent() <= 0) {
				broadcast(getroffenesSchiff.getSchiffsname()
						+ ": Lebenserhaltungssysteme sind vollstaendig vernichtet worden\n");
				setLebenserhaltungssystemeInProzent(0);
			}
		}
	}

	/**
	 * zur Ausgabe einer Nachricht an alle
	 * 
	 * @param nachricht Text, der uebermittelt werden soll
	 */
	public void broadcast(String nachricht) {
		if (broadcastKommunikator == null) {
			broadcastKommunikator = new ArrayList<String>();
		}
		broadcastKommunikator.add(nachricht);
	}

	/**
	 * zur Ausgabe des Logbuchs
	 * 
	 * @return Liste mit gespeicherten Nachrichten, die an alle geschickt wurden
	 */
	public static ArrayList<String> logbuchAusgeben() {
		return broadcastKommunikator;
	}

	/**
	 * zum Einsetzen der Photonentorpedos, welche sich im Raumschiff befinden
	 * 
	 * @param anzTorpedos Torpedos, die zum Abschuss bereit gemacht werden sollen
	 */
	public void photonentorpedosLaden(int anzTorpedos) {
		boolean torpedosVorhanden = false;
		for (int i = 0; i < this.ladungsverzeichnis.size(); i++) {
			if (this.ladungsverzeichnis.get(i).getTyp() == "Photonentorpedo") {
				torpedosVorhanden = true;
				if (anzTorpedos > this.ladungsverzeichnis.get(i).getMenge()) {
					this.photonentorpedos += this.ladungsverzeichnis.get(i).getMenge();
					this.ladungsverzeichnis.get(i).setMenge(0);
				} else {
					this.photonentorpedos += anzTorpedos;
					int geladeneTorpedos = this.ladungsverzeichnis.get(i).getMenge();
					this.ladungsverzeichnis.get(i).setMenge(geladeneTorpedos - anzTorpedos);
				}
				System.out.println(photonentorpedos + " Photonentorpedo(s) eingesetzt\n");
				break;
			}
		}

		if (!torpedosVorhanden) {
			System.out.println("Keine Photonentorpedos gefunden!");
			broadcast("-=*Click*=-");
		}
	}

	/**
	 * zum Aufraemen des Ladungsverzeichnisses (Ladung mit der Menge null wird
	 * entfernt)
	 */
	public void ladungsverzeichnisAufraeumen() {
		for (int i = ladungsverzeichnis.size() - 1; i <= 0; i++) {
			if (ladungsverzeichnis.get(i).getMenge() == 0) {
				ladungsverzeichnis.remove(i);
			}
		}
	}

	/**
	 * wird zur Reparatur des Raumschiffes benoetigt
	 * 
	 * @param energieversorgung gibt an, ob die Energieversorgung repariert werden
	 *                          soll
	 * @param schutzschilde     gibt an, ob die Schutzschilde repariert werden
	 *                          sollen
	 * @param schiffshuelle     gibt an, ob die Schiffshuelle repariert werden soll
	 * @param anzAndroiden      Anzahl der Reparatur Androiden, die zur Reparatur
	 *                          eingesetzt werden sollen
	 */
	public void reparaturauftragSenden(boolean energieversorgung, boolean schutzschilde, boolean schiffshuelle,
			int anzAndroiden) {
		double reparaturwert = 0;
		Random random = new Random();
		int zufallszahl = random.nextInt(100 - 1) + 1;
		int zuReparierendeStrukturen = 0;

		if (energieversorgung) {
			zuReparierendeStrukturen++;
		}

		if (schutzschilde) {
			zuReparierendeStrukturen++;
		}

		if (schiffshuelle) {
			zuReparierendeStrukturen++;
		}

		if (anzAndroiden > getReparaturAndroiden()) {
			anzAndroiden = getReparaturAndroiden();
		}

		reparaturwert = (zufallszahl * Math.abs(anzAndroiden) / zuReparierendeStrukturen);

		if (energieversorgung) {
			energieversorgungInProzent += reparaturwert;
		}

		if (schutzschilde) {
			schutzschildeInProzent += reparaturwert;
		}

		if (schiffshuelle) {
			huelleInProzent += reparaturwert;
		}

	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	public double getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}
	/**
	 * zum Setzen der Energieversorgung
	 * falls der Wert kleiner als 0 ist, wird er auf 0 festgelegt
	 * @param energieversorgungInProzent neuer Wert der Energieversorgung (in Prozent)
	 */
	public void setEnergieversorgungInProzent(double energieversorgungInProzent) {
		if (energieversorgungInProzent < 0) {
			this.energieversorgungInProzent = 0;
		} else { 
			this.energieversorgungInProzent = energieversorgungInProzent;
		}
	}

	public double getSchutzschildeInProzent() {
		return schutzschildeInProzent;
	}
	
	/**
	 * zum Setzen des Wertes der Schutzschilde in Prozent
	 * falls dieser kleiner als 0 ist, wird der Wert auf 0 festgelegt
	 * @param schutzschildeInProzent neuer Wert der Schutzschilde (in Prozent)
	 */
	public void setSchutzschildeInProzent(double schutzschildeInProzent) {
		if (schutzschildeInProzent < 0) {
			this.schutzschildeInProzent = 0;
		} else {
			this.schutzschildeInProzent = schutzschildeInProzent;
		}
	}

	public double getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	/**
	 * zum Setzen des Wertes der Lebenserhaltungssysteme in Prozent
	 * falls dieser kleiner als 0 ist, wird der Wert auf 0 festgelegt
	 * @param lebenserhaltungssystemeInProzent neuer Wert der Lebenserhaltungssysteme (in Prozent)
	 */
	public void setLebenserhaltungssystemeInProzent(double lebenserhaltungssystemeInProzent) {
		if (lebenserhaltungssystemeInProzent < 0) {
			this.lebenserhaltungssystemeInProzent = 0;
		} else {
			this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		}
	}

	public double getHuelleInProzent() {
		return huelleInProzent;
	}
	
	/**
	 * zum Setzen des Wertes der Huelle in Prozent
	 * falls dieser kleiner als 0 ist, wird der Wert auf 0 festgelegt 
	 * @param huelleInProzent neuer Wert der Huelle (in Prozent)
	 */
	public void setHuelleInProzent(double huelleInProzent) {
		if (huelleInProzent < 0) {
			this.huelleInProzent = 0;
		} else {
			this.huelleInProzent = huelleInProzent;
		}
	}

	public int getPhotonentorpedos() {
		return photonentorpedos;
	}
	
	/**
	 * zum Setzen der Anzahl der Photonentorpedos
	 * falls dieser kleiner als 0 ist, wird der Wert auf 0 festgelegt
	 * @param photonentorpedos neue Anzahl der Photonentorpedos
	 */
	public void setPhotonentorpedos(int photonentorpedos) {
		if (photonentorpedos < 0) {
			this.photonentorpedos = 0;
		} else {
			this.photonentorpedos = photonentorpedos;
		}
	}

	public int getReparaturAndroiden() {
		return reparaturAndroiden;
	}
	
	/**
	 * zum Setzen der Anzahl der Reparatur Androiden
	 * falls dieser kleiner als 0 ist, wird der Wert auf 0 festgelegt
	 * @param reparaturAndroiden neue Anzahl der Reparatur Androiden
	 */
	public void setReparaturAndroiden(int reparaturAndroiden) {
		if (reparaturAndroiden < 0) {
			this.reparaturAndroiden = 0;
		} else {
			this.reparaturAndroiden = reparaturAndroiden;
		}
	}

	public List<Ladung> getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}

	public void setLadungsverzeichnis(List<Ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}

	public static ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}

	public static void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		Raumschiff.broadcastKommunikator = broadcastKommunikator;
	}

	public ArrayList<String> getLogbuch() {
		return logbuch;
	}

	public void setLogbuch(ArrayList<String> logbuch) {
		this.logbuch = logbuch;
	}

}

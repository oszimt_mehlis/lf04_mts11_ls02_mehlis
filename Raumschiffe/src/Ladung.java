
/**
 * Klasse zur Verwaltung von Ladungen
 * @author Christoph
 *
 */
public class Ladung {

	private String typ;
	private int menge;

	/**
	 * Standardkonstruktor der Klasse Ladung
	 */
	public Ladung() {

	}

	/**
	 * vollparametisierter Konstruktor der Klasse Ladung
	 * @param typ Art/Name der Ladung
	 * @param menge Anzahl/Menge der Ladung
	 */
	public Ladung(String typ, int menge) {
		setTyp(typ);
		setMenge(menge);
	}


	@Override
	public String toString() {
		return "Ladung [typ=" + typ + ", menge=" + menge + "]";
	}
	
	public String getTyp() {
		return typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public int getMenge() {
		return menge;
	}
	
	/**
	 * zum Setzen der Menge, falls diese kleiner als 0 ist, wird sie automatisch auf 0 gesetzt
	 * @param menge neue Menge der Ladung
	 */
	public void setMenge(int menge) {
		if (menge < 0) {
			this.menge = 0;
		} else {
			this.menge = menge;
		}
	}

}
